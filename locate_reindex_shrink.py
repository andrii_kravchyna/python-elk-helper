from datetime import date, timedelta

import requests
import configparser
from elasticsearch import Elasticsearch

import os
script_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep

today = date.today()
print(today)
first = today.replace(day=1)
print(first)
lastMonth = first - timedelta(days=1)
firstDayLastMonth = lastMonth.replace(day=1)
print(lastMonth.strftime("%Y-%m"))

last2Month = firstDayLastMonth - timedelta(days=1)
# print(last2Month.strftime("%Y-%m"))

last2Month_str = last2Month.strftime("%Y.%m")
print(last2Month_str)

firstDayLast2Month = last2Month.replace(day=1)
print(lastMonth.strftime("%Y-%m"))

last3Month = firstDayLast2Month - timedelta(days=1)
last3Month_str = last3Month.strftime("%Y.%m")
print(last3Month_str)


for date_for_search in last3Month_str,last2Month_str:

    index_SEARCH_by_date = '*' + date_for_search + '.*0000*'

    config = configparser.ConfigParser()
    config.read(script_dir+'config.ini')

    es = Elasticsearch([{'host': config['Elasticsearch']['host']
                            , 'port': config['Elasticsearch']['port']}]
                       ,timeout=3000, max_retries=10, retry_on_timeout=True)
    target = es.indices.get(index_SEARCH_by_date)
    for t in target:
        print(t)

        target_index = t.split(date_for_search)

        # for res_date in target_index:
        #     print(res_date)
        print(target_index[0])
        target_index_name = target_index[0]

        index_name = target_index_name + date_for_search

        index_name_SEARCH = index_name + '.*0000*'
        index_name_REINDEX = index_name + '----reindex'

        target1 = es.indices.get(index_name_SEARCH)
        for t1 in target1:
            print(t1)
        print(len(target1))
        if len(target1) > 0:
            result = es.reindex({
                "source": {"index": index_name_SEARCH},
                "dest": {"index": index_name_REINDEX}
            }, timeout='1h', wait_for_completion=True, requests_per_second = 10)

            print(result)

            if result['total'] and result['took'] and not result['timed_out']:
                print("Seems reindex was successfull!")

                result = es.indices.put_settings('{"settings": {"index.blocks.write": true}}'
                                                 , index_name_REINDEX)
                print(result)

                result = es.indices.shrink(index_name_REINDEX, index_name, '''
                {
                    "settings": {
                    "index.number_of_replicas": 0,
                    "index.number_of_shards": 1,
                    "index.codec": "best_compression"
                }
                }
                ''', timeout='1h', wait_for_active_shards=1)

                print(result)

                result = es.indices.forcemerge(index_name, max_num_segments=1, flush=True)

                print(result)

                print("going to delete the old index!")

                es.indices.delete(index_name_REINDEX)
                es.indices.delete(index_name_SEARCH)

        exit()
