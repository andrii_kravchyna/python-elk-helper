### Purpose
simple script to merge and shrink indices one by one

### Config example

`config.ini`
```ini
[Elasticsearch]
host = elk.example.local
port = 9200
```

python3 needed

### How to use:
install dependencies from setup.md

launch
```shell script
/usr/bin/python3.5 /home/k--andrey/Projects/Pycharm/elk_ops/locate_reindex_shrink.py
```